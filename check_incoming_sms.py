import pymysql
import requests
from icecream import ic


"""Скрипт для отправки входящих сообщений в телеграмм. Для работы нужно заполнить Settings и желательно поставить в крон """ 
class Settings:
	""" Настройки для работы скрипта """
	telegram_bot_token = '610404600:AAETdM1kxbhZ8rbhFfWvo9d6kURm1D5JTyg'	 	# Токен бота
	telegram_group_id = '-1001571886866'		# ID канала или группы
	db_host = '192.168.0.58'	# IP хоста с базой
	db_port = 33338				# Порт на котором живёт база
	db_user = 'root'			# Пользователь базы
	db_password = '123456'		# Пароль
	db = 'goip'				# Название базы
###########################################################################
def get_con():
	""" Подключения к базе """
	connection = pymysql.connect(host=Settings.db_host,	port=Settings.db_port, user=Settings.db_user, password=Settings.db_password, db=Settings.db)
	cursor = connection.cursor()
	return connection, cursor


def get_new_sms():
	""" Получение непрочитанных смс """
	connection, cursor = get_con()
	cursor.execute("SELECT goipname, srcnum, msg, senttime, id FROM receive WHERE `read` = '0'")
	results = cursor.fetchall()
	connection.close()
	return results	


def update_status(sms_id):
	""" Обновляем статус смс """
	connection, cursor = get_con()	
	cursor.execute(f"UPDATE `receive` SET `read` = 1 WHERE `id` = {sms_id}")
	connection.commit()
	connection.close()


def parse_sms(incoming_sms):
	""" Разбор смс """
	for goipname, sender, text, senttime, sms_id in incoming_sms:
		goipname = goipname.replace('_', '-')
		try:
			update_status(sms_id)
		except:
			return
		telegram(f"""SMS на номер: *{goipname}*
Отправитель: {sender}
=====================================
*{text}*""")


def telegram(text):
	""" Отправка сообщений в телеграм """
	params = dict(chat_id = Settings.telegram_group_id, text = text, parse_mode='Markdown')								# Формируем параметры
	e = requests.post('https://api.telegram.org/bot{}/sendMessage'.format(Settings.telegram_bot_token), params=params)		# Отпрака сообщения
	print(e.text)
if __name__ == '__main__':
	incoming_sms = get_new_sms() 
#	for sms in incoming_sms:
#		print(sms)
	parse_sms(incoming_sms)



